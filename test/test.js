#!/usr/bin/env node

/* jslint node:true */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    superagent = require('superagent'),
    util = require('util');

var by = require('selenium-webdriver').By,
    until = require('selenium-webdriver').until,
    Key = require('selenium-webdriver').Key,
    Builder = require('selenium-webdriver').Builder;

describe('Application life cycle test', function () {
    this.timeout(0);
    var server, browser = new Builder().forBrowser('chrome').build();
    var LOCATION = 'test';
    var app;
    var username = process.env.USERNAME;
    var password = process.env.PASSWORD;
    var TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 15000;
    var token;
    var mediaLink;

    before(function (done) {
        if (!process.env.USERNAME) return done(new Error('USERNAME env var not set'));
        if (!process.env.PASSWORD) return done(new Error('PASSWORD env var not set'));

        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();

        done();
    });

    after(function (done) {
        browser.quit();
        server.stop();
        done();
    });

    function login(username, password, done) {
        browser.manage().deleteAllCookies().then(function () {
            return browser.get('https://' + app.fqdn + '/wp-login.php');
        }).then(function () {
            return browser.sleep(2000); // there seems to be some javascript that gives auto-focus to username
        }).then(function () {
            return browser.findElement(by.id('user_login')).sendKeys(username);
        }).then(function () {
            return browser.findElement(by.id('user_pass')).sendKeys(password);
        }).then(function () {
            return browser.findElement(by.tagName('form')).submit();
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//h1[text()="Dashboard"]')), TIMEOUT);
        }).then(function () {
            done();
        });
    }

    function logout(done) {
        browser.manage().deleteAllCookies().then(function () {
            browser.executeScript('localStorage.clear();');
            browser.executeScript('sessionStorage.clear();');

            done();
        });
    }

    function checkNonAdminDashboard(done) {
        browser.get('https://' + app.fqdn + '/wp-admin/').then(function () {
            return browser.sleep(3000);
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//div[@class="wp-menu-name" and text()="Profile"]')), TIMEOUT);
        }).then(function () {
            done();
        });
    }

    function checkCredentials(done) {
        var out = execSync(util.format('cloudron exec --app %s -- cat /app/data/credentials.txt', app.id));
        expect(out.toString('utf8').indexOf('MySQL Credentials')).to.not.be(-1);
        done();
    }

    function checkPost(done) {
        browser.get('https://' + app.fqdn).then(function () {
            if (app.manifest.version === '1.7.0') {
                return browser.wait(until.elementLocated(by.xpath('//h3/a[text()="Hello Cloudron!"]')), TIMEOUT);
            } else {
                return browser.wait(until.elementLocated(by.xpath('//h2/a[text()="Hello Cloudron!"]')), TIMEOUT);
            }
        }).then(function () {
            done();
        });
    }

    function editPost(disableTips, done) {
        browser.get('https://' + app.fqdn).then(function () {
            return browser.sleep(3000);
        }).then(function () {
            return browser.get('https://' + app.fqdn + '/wp-admin/post.php?post=1&action=edit');
        }).then(function () {
            if (!disableTips) return Promise.resolve();
            return browser.wait(until.elementLocated(by.xpath('//button[@aria-label="Close dialog"]')), TIMEOUT);
        }).then(function () {
            if (!disableTips) return Promise.resolve();
            return browser.findElement(by.xpath('//button[@aria-label="Close dialog"]')).click();
        }).then(function () {
            return browser.findElement(by.xpath('//textarea[@id="post-title-0"]')).sendKeys(Key.chord(Key.CONTROL, 'a'));
        }).then(function () {
            return browser.findElement(by.xpath('//textarea[@id="post-title-0"]')).sendKeys('Hello Cloudron!');
        }).then(function () {
            return browser.findElement(by.xpath('//button[text()="Update"]')).click();
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//*[contains(text(), "Post updated.")]')), TIMEOUT);
        }).then(function () {
            return browser.sleep(3000);
        }).then(function () {
            done();
        });
    }


    function uploadMedia(done) {
        browser.get('https://' + app.fqdn + '/wp-admin/media-new.php?browser-uploader').then(function () {
            return browser.wait(until.elementLocated(by.id('async-upload')), TIMEOUT);
        }).then(function () {
            return browser.findElement(by.xpath('//input[@id="async-upload" and @type="file"]')).sendKeys(path.resolve(__dirname, '../logo.png'));
        }).then(function () {
            return browser.findElement(by.id('html-upload')).click();
        }).then(function () {
            return browser.wait(function () {
                return browser.getCurrentUrl().then(function (url) {
                    return url === 'https://' + app.fqdn + '/wp-admin/upload.php';
                });
            }, TIMEOUT);
        }).then(function () {
            done();
        });
    }

    function checkMedia(item, done) {
        browser.get(`https://${app.fqdn}/wp-admin/upload.php?item=${item}`).then(function () { // there's got to be a better way..
            if (app.manifest.version === '2.3.1') {
                return browser.wait(until.elementLocated(by.xpath('//*[text()="Attachment Details"]')), TIMEOUT);
            } else {
                return browser.wait(until.elementLocated(by.xpath('//*[text()="Attachment details"]')), TIMEOUT);
            }
        }).then(function () {
            return browser.findElement(by.xpath('//img[@class="details-image"]')).getAttribute('src');
        }).then(function (srcLink) {
            console.log('media is located at ', srcLink);
            mediaLink = srcLink;
            done();
        });
    }

    function checkMediaLink(done) {
        superagent.get(mediaLink).end(function (error, result) {
            expect(error).to.be(null);
            expect(result.statusCode).to.be(200);
            done();
        });
    }

    function checkMailConfiguration(done) {
        browser.get(`https://${app.fqdn}/wp-admin/admin.php?page=wp-mail-smtp&tab=test`).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//button[contains(text(),"Send Email")]')), TIMEOUT);
        }).then(function () {
            return browser.findElement(by.xpath('//button[contains(text(),"Send Email")]')).click();
        }).then(function () {
            return browser.sleep(4000);
        // TODO: the selector doesn't work for some reason
        // }).then(function () {
        //     return browser.wait(until.elementLocated(by.xpath('//*[contains(text(), "email was sent successfully")]')), TIMEOUT);
        }).then(function () {
            done();
        });
    }

    xit('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can login', function (done) {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        superagent.post('https://' + inspect.apiEndpoint + '/api/v1/developer/login').send({
            username: process.env.USERNAME,
            password: process.env.PASSWORD
        }).end(function (error, result) {
            if (error) return done(error);
            if (result.statusCode !== 200) return done(new Error('Login failed with status ' + result.statusCode));

            token = result.body.accessToken;

            superagent.get('https://' + inspect.apiEndpoint + '/api/v1/profile')
                .query({ access_token: token }).end(function (error, result) {
                    if (error) return done(error);
                    if (result.statusCode !== 200) return done(new Error('Get profile failed with status ' + result.statusCode));

                    done();
                });
        });
    });

    it('install app', function () {
        execSync('cloudron install --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', function () {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];

        expect(app).to.be.an('object');
    });

    it('can get the main page', function (done) {
        superagent.get('https://' + app.fqdn).end(function (error, result) {
            expect(error).to.be(null);
            expect(result.status).to.eql(200);

            done();
        });
    });

    it('can admin login', login.bind(null, 'admin', 'changeme'));

    it('is an admin dashboard', function (done) {
        browser.wait(until.elementLocated(by.xpath('//div[@class="wp-menu-name" and contains(text(), "Plugins")]')), TIMEOUT).then(function () { done(); });
    });

    it('can edit', editPost.bind(null, true));
    it('can upload media', uploadMedia);
    it('can see media', checkMedia.bind(null, 6));
    it('can see media link', checkMediaLink);
    it('can see credentials', checkCredentials);
    it('can send test mail', checkMailConfiguration);
    it('can logout', logout);

    it('can ldap login', login.bind(null, username, password));
    it('is non-admin dashboard', checkNonAdminDashboard);
    it('can logout', logout);

    it('can restart app', function (done) {
        execSync('cloudron restart --app ' + app.id);
        done();
    });

    it('can admin login', login.bind(null, 'admin', 'changeme'));
    it('can see updated post', checkPost);
    it('can see media link', checkMediaLink);
    it('can see credentials', checkCredentials);
    it('can logout', logout);

    it('can ldap login', login.bind(null, username, password));
    it('is non-admin dashboard', checkNonAdminDashboard);
    it('can logout', logout);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        execSync('cloudron install --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can admin login', login.bind(null, 'admin', 'changeme'));
    it('can see updated post', checkPost);
    it('can see media link', checkMediaLink);
    it('can see credentials', checkCredentials);
    it('can admin login', login.bind(null, 'admin', 'changeme'));

    it('runs cron jobs', function (done) {
        this.timeout(6 * 60 * 1000); // cron runs only every 5 minutes
        console.log('It can take upto 6 mins to detect that cron is working');

        function checkLogs() {
            var logs = execSync('cloudron logs --lines 1000 --app ' + app.id).toString('utf8');
            if (logs.indexOf('Success: Executed a total of') !== -1) { console.log(); return done(); }

            process.stdout.write('.');
            setTimeout(checkLogs, 45000);
        }

        setTimeout(checkLogs, 45000);
    });

    it('can logout', logout);

    it('can ldap login', login.bind(null, username, password));
    it('is non-admin dashboard', checkNonAdminDashboard);
    it('can logout', logout);

    it('move to different location', function (done) {
        browser.manage().deleteAllCookies();

        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
            var inspect = JSON.parse(execSync('cloudron inspect'));
            app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
            expect(app).to.be.an('object');
            mediaLink = mediaLink.replace(LOCATION, LOCATION + '2');

            done();
        });
    });

    it('can admin login', login.bind(null, 'admin', 'changeme'));
    it('can see updated post', checkPost);
    it('can see media link', checkMediaLink);
    it('can see credentials', checkCredentials);
    it('can admin login', login.bind(null, 'admin', 'changeme'));
    it('can logout', logout);

    it('can ldap login', login.bind(null, username, password));
    it('is non-admin dashboard', checkNonAdminDashboard);
    it('can logout', logout);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    // No SSO
    it('install app (no sso)', function () {
        execSync('cloudron install --no-sso --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', function () {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];

        expect(app).to.be.an('object');
    });

    it('can login (no sso)', login.bind(null, 'admin', 'changeme'));

    it('is an admin dashboard (no sso)', function (done) {
        browser.wait(until.elementLocated(by.xpath('//div[@class="wp-menu-name" and contains(text(), "Plugins")]')), TIMEOUT).then(function () { done(); });
    });

    it('can logout', logout);

    it('uninstall app (no sso)', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    // test update
    it('can install app - update', function () {
        execSync('cloudron install --appstore-id org.wordpress.unmanaged.cloudronapp --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    });
    it('can admin login', login.bind(null, 'admin', 'changeme'));
    it('can edit', editPost.bind(null, false));
    it('can upload media', uploadMedia);

    it('can update', function () {
        execSync('cloudron update --app ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can admin login', login.bind(null, 'admin', 'changeme'));
    it('can see updated post', checkPost);
    it('can see media', checkMedia.bind(null, 6));
    it('can see media link', checkMediaLink);
    it('can see credentials', checkCredentials);
    it('can logout', logout);

    it('can ldap login', login.bind(null, username, password));
    it('is non-admin dashboard', checkNonAdminDashboard);
    it('can logout', logout);

    it('uninstall app', function (done) {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        done();
    });
});

