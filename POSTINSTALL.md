This app is pre-setup with an admin account. The initial credentials are:

**Username**: admin<br/>
**Password**: changeme<br/>

Please change the password immediately.

SFTP credentials can be obtained from the app's `Access Control` view.

