#!/bin/bash

set -eu

wp='sudo -u www-data -i -- /app/pkg/wp --path=/app/data/public/ --skip-themes --skip-plugins'

mkdir -p /app/data/apache /run/apache2 /run/sessions

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

[[ ! -f /app/data/apache/mpm_prefork.conf ]] && cp /app/pkg/mpm_prefork.conf /app/data/apache/mpm_prefork.conf

if [[ ! -d "/app/data/public" ]]; then
    cp -rf /app/code/wordpress /app/data/public
    chown -R www-data:www-data /app/data

    echo "=> Create initial WordPress config"
    $wp config create --dbname="${CLOUDRON_MYSQL_DATABASE}" --dbuser="${CLOUDRON_MYSQL_USERNAME}" --dbpass="${CLOUDRON_MYSQL_PASSWORD}" --dbhost="${CLOUDRON_MYSQL_HOST}:${CLOUDRON_MYSQL_PORT}"

    echo "=> Install WordPress"
    # --skip-email is part of 0.23.0 https://github.com/wp-cli/wp-cli/pull/2345 and https://github.com/wp-cli/wp-cli/issues/1164
    $wp core install --skip-email --url="${CLOUDRON_APP_ORIGIN}" --title="My website" --admin_user="admin" --admin_password="changeme" --admin_email="${CLOUDRON_MAIL_FROM}"

    echo "=> Install smtp mail plugin"
    $wp plugin install /app/pkg/wp-mail-smtp.zip
    $wp plugin activate wp-mail-smtp

    echo "==> Install redis plugin"
    $wp plugin install /app/pkg/wp-redis-cache.zip
    $wp plugin activate redis-cache

    if [[ -n "${CLOUDRON_LDAP_SERVER:-}" ]]; then
        echo "==> Install ldap plugin"
        $wp plugin install --force /app/pkg/authLdap.zip
        $wp plugin activate authLdap-2.4.2
    fi

    $wp config set WP_REDIS_HOST "${CLOUDRON_REDIS_HOST}"
    $wp config set WP_REDIS_PORT "${CLOUDRON_REDIS_PORT}"
    $wp config set WP_REDIS_PASSWORD "${CLOUDRON_REDIS_PASSWORD}"

    $wp config set --raw DISABLE_WP_CRON true

    # this sets up a random cookie name
    random=$(pwgen -1s 32)
    $wp config set --raw COOKIEHASH "md5('${random}')"

    $wp config set --raw WP_DEBUG false
    $wp config set --raw WP_DEBUG_LOG false
    $wp config set --raw WP_DEBUG_DISPLAY false

    # this copies over the object-cache.php file into wp core
    sudo -u www-data -i -- /app/pkg/wp --path=/app/data/public/ --skip-themes redis enable

    chown -R www-data:www-data /app/data
fi

echo "=> Changing permissions"
chown -R www-data:www-data /app/data /run

# Update db settings first. otherwise, the domain/mail changes will overwrite the original db when cloning
echo "=> Updating db settings"
$wp config set DB_HOST "${CLOUDRON_MYSQL_HOST}:${CLOUDRON_MYSQL_PORT}"
$wp config set DB_NAME "${CLOUDRON_MYSQL_DATABASE}"
$wp config set DB_USER "${CLOUDRON_MYSQL_USERNAME}"
$wp config set DB_PASSWORD "${CLOUDRON_MYSQL_PASSWORD}"

echo "=> Updating redis settings"
$wp config set WP_REDIS_HOST "${CLOUDRON_REDIS_HOST}"
$wp config set WP_REDIS_PORT "${CLOUDRON_REDIS_PORT}"
$wp config set WP_REDIS_PASSWORD "${CLOUDRON_REDIS_PASSWORD}"

# configure WP mail smtp plugin
echo "==> Configuring smtp mail"
if mail_config=$($wp --format=json option get wp_mail_smtp); then
    mail_from_name=$(echo "${mail_config}" | jq -r .mail.from_name)
else
    mail_from_name="WordPress"
fi

mailConfig=$(cat <<EOF
{
    "mail": {
        "from_email"        : "${CLOUDRON_MAIL_FROM}",
        "from_name"         : "${mail_from_name}",
        "mailer"            : "smtp",
        "return_path"       : "from_email_force",
        "from_name_force"   : true,
        "from_email_force"  : true
    },
    "smtp": {
        "autotls"           : true,
        "host"              : "${CLOUDRON_MAIL_SMTP_SERVER}",
        "encryption"        : "none",
        "port"              : ${CLOUDRON_MAIL_SMTP_PORT},
        "auth"              : true,
        "user"              : "${CLOUDRON_MAIL_SMTP_USERNAME}",
        "pass"              : "${CLOUDRON_MAIL_SMTP_PASSWORD}"
    }
}
EOF
)

if [[ -n "${CLOUDRON_LDAP_SERVER:-}" ]]; then
    # configure LDAP
    # https://github.com/heiglandreas/authLdap/blob/master/authLdap.php#L644

    # GroupEnable means that cloudron groups are carried over to wp groups
    # GroupOverUser means that if there is an existing wp group for the user, it won't be overwritten
    # The above implies that users can override the roles in wordpress and it
    # doesn't get overwritten on re-login
    echo "==> Configuring LDAP"

    if ldap_config=$($wp --format=json option get authLDAPOptions); then
        default_role=$(echo "${ldap_config}" | jq -r .DefaultRole)
    else
        default_role="editor"
    fi

    ldapConfig=$(cat <<EOF
    {
        "Enabled"       : true,
        "CachePW"       : false,
        "URI"           : "ldap://${CLOUDRON_LDAP_SERVER}:${CLOUDRON_LDAP_PORT}/${CLOUDRON_LDAP_USERS_BASE_DN}",
        "Filter"        : "(username=%s)",
        "NameAttr"      : "givenName",
        "SecName"       : "sn",
        "UidAttr"       : "username",
        "MailAttr"      : "mail",
        "WebAttr"       : "",
        "Debug"         : false,
        "DefaultRole"   : "${default_role}",
        "GroupEnable"   : false,
        "GroupOverUser" : false,
        "Version"       : 1
    }
EOF
)
    $wp --format=json option update authLDAPOptions "${ldapConfig}"
fi

# note that is-installed only works if db settings are up to date
if $wp core is-installed; then
    echo "=> Updating domain related settings"
    # This keeps the values in Settings -> Site/WordPress Address read-only
    $wp config set WP_HOME "${CLOUDRON_APP_ORIGIN}"
    $wp config set WP_SITEURL "${CLOUDRON_APP_ORIGIN}"

    # This is only done for keeping the db dumps more useful
    $wp option update siteurl "${CLOUDRON_APP_ORIGIN}"
    $wp option update home "${CLOUDRON_APP_ORIGIN}"

    echo "=> Updating mailer settings"
    $wp --format=json option update wp_mail_smtp "${mailConfig}"
else
    echo "=> WordPress does not seem to be working or is not installed"
fi

echo "=> Creating credentials.txt"
sudo -u www-data -- sed -e "s,MYSQL_HOST,${CLOUDRON_MYSQL_HOST}," \
    -e "s,MYSQL_PORT,${CLOUDRON_MYSQL_PORT}," \
    -e "s,MYSQL_USERNAME,${CLOUDRON_MYSQL_USERNAME}," \
    -e "s,MYSQL_PASSWORD,${CLOUDRON_MYSQL_PASSWORD}," \
    -e "s,MYSQL_DATABASE,${CLOUDRON_MYSQL_DATABASE}," \
    -e "s,MYSQL_URL,${CLOUDRON_MYSQL_URL}," \
    -e "s,MAIL_SMTP_SERVER,${CLOUDRON_MAIL_SMTP_SERVER}," \
    -e "s,MAIL_SMTP_PORT,${CLOUDRON_MAIL_SMTP_PORT}," \
    -e "s,MAIL_SMTPS_PORT,${CLOUDRON_MAIL_SMTPS_PORT}," \
    -e "s,MAIL_SMTP_USERNAME,${CLOUDRON_MAIL_SMTP_USERNAME}," \
    -e "s,MAIL_SMTP_PASSWORD,${CLOUDRON_MAIL_SMTP_PASSWORD}," \
    -e "s,MAIL_FROM,${CLOUDRON_MAIL_FROM}," \
    -e "s,MAIL_DOMAIN,${CLOUDRON_MAIL_DOMAIN}," \
    -e "s,REDIS_SERVER,${CLOUDRON_REDIS_HOST}," \
    -e "s,REDIS_PORT,${CLOUDRON_REDIS_PORT}," \
    -e "s,REDIS_PASSWORD,${CLOUDRON_REDIS_PASSWORD}," \
    /app/pkg/credentials.template > /app/data/credentials.txt

echo "=> Starting apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
