This app packages WordPress <upstream>5.5.3 (PHP 7.4)</upstream>.

WordPress (Developer) package gives you complete control over your WordPress installation.
Unlike the WordPress (Managed) package, core WordPress files are editable. Because the core files
are editable, you have to keep WordPress updated from the WP Admin dashboard. Some salient features are:

* Supports all migration plugins. Use this package to migrate your existing site into Cloudron.
* Make changes to WordPress via SFTP or the File Manager.
* Supports all WordPress security plugins like WordFence.
* Update WordPress from inside WordPress' admin dashboard.

## About

WordPress is web software you can use to create a beautiful website or blog.
We like to say that WordPress is both free and priceless at the same time.

The core software is built by hundreds of community volunteers, and when
you’re ready for more there are thousands of plugins and themes available
to transform your site into almost anything you can imagine. Over 60 million
people have chosen WordPress to power the place on the web they call “home”
— we’d love you to join the family.

### Apps

* [Android](https://play.google.com/store/apps/details?id=org.wordpress.android&hl=en)
* [iOS](https://itunes.apple.com/us/app/wordpress/id335703880?mt=8&uo=6&at=&ct=)
