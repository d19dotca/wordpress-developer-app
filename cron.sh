#!/bin/bash

set -eu

wp='sudo -u www-data -i -- /app/pkg/wp --skip-themes --path=/app/data/public/'

echo "=> Run cron job"
$wp cron event run --due-now
